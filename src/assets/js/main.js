;'use strict';

(function($){
    
    $(document).ready(function(){
        var $buttonTop = $('#button-top');

        $buttonTop.click(function(ev){
            ev.preventDefault();

            var $curr    = $(this),
                $content = $($curr.attr('href')),
                dest     = $content.offset().top;

            $("html, body").animate({
                scrollTop: dest
            }, 1000);
        });
    });
    
})(jQuery);