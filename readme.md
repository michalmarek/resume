Resume - Michal Marek
=======================
This project is for my personal resume.

Installation
------------
You need to proceed steps below for correct installation.

1. Install [node.js](https://nodejs.org/en/) on your machine
2. Type into your command line within project folder
```
npm install
npm install -g bower;
bower install;
```

Launching
------------
```
gulp serve
```

Building
------------
```
gulp build
```

Deploy
------------
```
gulp deploy --psw 1234
```