'use strict';

//
//  Dependencies
//
var gulp    = require('gulp'),
    wiredep = require('wiredep').stream,
    gutil   = require('gulp-util'),
    $       = require('gulp-load-plugins')({ pattern: [
        'del',
        'path',
        'browser-sync',
        'main-bower-files',
        'gulp-*'
    ]});



//
//  Config
//
var config = {
    paths: {
        src:   'src',
        dist:  'dist',
        tmp:   '.tmp',
        bower: 'bower_components'
    },
    routes: {
        '/bower_components': 'bower_components'
    },
    deploy: {
        host: 'ftp.michalmarek.sk',
        user: 'michalmarek.sk',
        directory: '/sub/cv'
    }
};



//
//  Default
//
gulp.task('default', ['clean'], function(){
    gulp.start('build');
});



//
//  Clean
//
gulp.task('clean', function(){
    return $.del([$.path.join(config.paths.dist, '/'), $.path.join(config.paths.tmp, '/')]);
});



//
//  Build
//
gulp.task('build', ['build:html', 'build:fonts', 'build:others']);

gulp.task('build:html', ['inject'], function(){
    return gulp.src($.path.join(config.paths.tmp, '/serve/*.html'))
        .pipe($.useref())
        .pipe($.if('*.js', $.uglify()))
        .pipe($.if('*.css', $.cleanCss({keepSpecialComments: 0})))
        .pipe(gulp.dest($.path.join(config.paths.dist, '/')));
});

gulp.task('build:fonts', function(){
    var bowerComponents = gulp.src($.mainBowerFiles(), {base: config.paths.bower}),
        filterFonts     = $.filter('**/*.+(eot|svg|ttf|woff|woff2)');

    return bowerComponents
        .pipe(filterFonts)
        .pipe($.rename({dirname: ''}))
        .pipe(gulp.dest($.path.join(config.paths.dist, '/assets/fonts')));
});

gulp.task('build:others', function(){
    var imgFilter = $.filter('**/*.+(jpg|jpeg|png|gif)', {restore: true});

    return gulp.src([
        $.path.join(config.paths.src, '/**/*'),
        $.path.join(config.paths.src, '/**/'),
        $.path.join('!' + config.paths.src, '/**/*.{html,css,js,scss}')
    ])
        .pipe(imgFilter)
        // .pipe(require('gulp-tinypng-compress')({key: 'A3Aq7IV8vnPMTssD9EEO-wP86RB0XSAl', summarize: true}))
        .pipe(imgFilter.restore)
        .pipe(gulp.dest($.path.join(config.paths.dist, '/')));
});



//
//  Watch
//
gulp.task('watch', ['inject'], function(){
    gulp.watch([$.path.join(config.paths.src, '/**/*.html'), 'bower.json'], ['inject-reload']);

    gulp.watch($.path.join(config.paths.src, '/**/*.scss'), function(event) {
        if(event.type === 'changed') {
            gulp.start('styles-reload');
        } else {
            gulp.start('inject-reload');
        }
    });

    gulp.watch($.path.join(config.paths.src, '/**/*.js'), function(event) {
        if(event.type === 'changed') {
            gulp.start('scripts-reload');
        } else {
            gulp.start('inject-reload');
        }
    });

    gulp.watch([
        $.path.join(config.paths.src, '/**/*'),
        $.path.join('!' + config.paths.src, '/**/*.{html,css,js,scss}')
    ], function(event){
        if (event.type === 'deleted') {
            var sourcePath = $.path.relative($.path.resolve(config.paths.src), event.path);
            $.del($.path.join(config.paths.tmp, '/serve', sourcePath));
        } else {
            gulp.start('others-reload');
        }
    });
});



//
//  TMP - Inject
//
gulp.task('inject', ['generate:styles', 'generate:scripts', 'generate:others'], function(){
    var injectStyles  = gulp.src($.path.join(config.paths.tmp, '/serve/**/*.css'), {read: false});
    var injectScripts = gulp.src($.path.join(config.paths.tmp, '/serve/**/*.js'), {read: false});

    var injectOptions = {
        ignorePath: $.path.join(config.paths.tmp, '/serve'),
        addRootSlash: false
    };

    return gulp.src($.path.join(config.paths.src, '/index.html'))
        .pipe($.inject(injectStyles, injectOptions))
        .pipe($.inject(injectScripts, injectOptions))
        .pipe(wiredep())
        .pipe(gulp.dest($.path.join(config.paths.tmp, '/serve')));
});



//
//  TMP - Styles
//
gulp.task('generate:styles', function(){
    var sassOptions = {
        style: 'expanded'
    };

    return gulp.src([$.path.join(config.paths.src, '/assets/css/app.scss')])
        .pipe($.plumber())
        .pipe($.sass(sassOptions).on('error', $.sass.logError))
        .pipe(gulp.dest($.path.join(config.paths.tmp, '/serve/assets/css')));
});



//
//  TMP - Scripts
//
gulp.task('generate:scripts', function(){
    return gulp.src([$.path.join(config.paths.src, '/assets/js/**/*.js')])
        .pipe(gulp.dest($.path.join(config.paths.tmp, '/serve/assets/js')));
});



//
//  TMP - Others
//
gulp.task('generate:others', function(){
    return gulp.src([
        $.path.join(config.paths.src, '/**/*'),
        $.path.join('!' + config.paths.src, '/**/*.{html,css,js,scss}')
    ]).pipe(gulp.dest($.path.join(config.paths.tmp, '/serve')));
});



//
//  Reloads
//
gulp.task('inject-reload', ['inject'], function(){
    $.browserSync.reload();
});

gulp.task('styles-reload', ['generate:styles'], function(){
    $.browserSync.reload();
});

gulp.task('scripts-reload', ['generate:scripts'], function(){
    $.browserSync.reload();
});

gulp.task('others-reload', ['generate:others'], function(){
    $.browserSync.reload();
});



//
//  Serve
//
function browserSyncInit(baseDir, browser){
    browser    = (browser === undefined ? 'default' : browser);
    var routes = (baseDir === $.path.join(config.paths.tmp, '/serve')) ? config.routes : null;

    $.browserSync.init({
        server: {
            baseDir: baseDir,
            routes: routes
        },
        browser: browser
    });
}

gulp.task('serve', ['watch'], function(){
    browserSyncInit($.path.join(config.paths.tmp, '/serve'));
});

gulp.task('serve:dist', ['build'], function(){
    browserSyncInit(config.paths.dist);
});



//
//  Deploy
//
gulp.task('deploy', ['build'], function(){
    var pswIndex = process.argv.indexOf("--psw") + 1,
        psw      = process.argv[pswIndex];

    var connection = require('vinyl-ftp').create({
        host: config.deploy.host,
        user: config.deploy.user,
        password: psw,
        parallel: 5,
        log: gutil.log
    });

    gulp.src($.path.join(config.paths.dist, '/**/*.*'))
        .pipe($.prompt.confirm({
            message: "Are you sure you want to deploy this project?",
            default: false
        }))
        .pipe(connection.newer(config.deploy.directory))
        .pipe(connection.dest(config.deploy.directory));
});